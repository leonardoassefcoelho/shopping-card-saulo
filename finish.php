<?php
	if (isset($_POST['submit'])) {
		$to = $_POST['email'];
		$from = "leoassefbrotas@gmail.com";
		$name = $_POST['name'];
		$address = $_POST['address'];
		$items = $_POST['items'];
		$subject = "Compra no site";

		$items = substr($items, 1, -1);
		$items = stripslashes($items);

		$items = json_decode($items);

		$items_string = "";

		foreach($items as $item) {
			$items_string .= $item->product . ' x ' . $item->qty . "\n\n";
		}

		$message = $name . " comprou:" . "\n\n" . $items_string . "\n\n" . "Os ítens serão enviados para: " . $address;

		$headers = "From:" . $from;

		$success = mail($to, $subject, $message, $headers);
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Carrinho de comprar - P1" />
	<title>Checkout - Carrinho - Saulo</title>
	<link rel="stylesheet" href="css/style.min.css" />
	<script src="js/jquery.min.js"></script>
	<script src="js/main.min.js"></script>
</head>

<body>
	<header>
		<div class="header padding-container">
			<div class="logo">
				<a href="index.html" aria-label="Home">
					<h1>logo</h1>
				</a>
			</div>

			<a aria-label="Carrinho" href="cart.html" class="cart-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="20.25" height="18" viewBox="0 0 20.25 18">
					<path d="M18.567,10.593l1.662-7.313a.844.844,0,0,0-.823-1.031H5.6L5.275.675A.844.844,0,0,0,4.448,0H.844A.844.844,0,0,0,0,.844v.563a.844.844,0,0,0,.844.844H3.3L5.77,14.324a1.969,1.969,0,1,0,2.357.3H15.5a1.968,1.968,0,1,0,2.236-.366l.194-.853a.844.844,0,0,0-.823-1.031H7.668l-.23-1.125H17.744A.844.844,0,0,0,18.567,10.593Z" style="fill: #fff;" />
				</svg>
			</a>
		</div>
	</header>

	<main>
		<section class="checkout container">
			<h2 class="section-title">Finalizar compra</h2>
			<form class="checkout-form" action="" method="post">
				<input required type="text" name="name" placeholder="Nome completo">
				<input required type="text" name="address" placeholder="Endereço">
				<input required type="email" name="email" placeholder="E-Mail">
				<input class="items" hidden type="text" name="items" placeholder="items">
				<input type="submit" name="submit" value="Comprar">
			</form>
		</section>
	</main>
	<footer>
		<div class="names container">
			<p class="name">Leonardo Assef Coelho - RA: 4200769</p>
			<p class="name">Rogério Martins - RA: 4200769</p>
		</div>
	</footer>

	<script type="text/javascript">
		cardLoad();

		var items = sessionStorage.getItem('cart');

		var items = JSON.stringify(items);

		$(".items").val(items);
	</script>
</body>

</html>