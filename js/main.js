function addProduct(name, price, imgLocation, qty) {
	var item = {
		product: name,
		price: price,
		qty: 1,
		imgLocation: imgLocation,
	};

	var actualItems = [];

	if (sessionStorage.getItem("cart")) {
		actualItems = sessionStorage.getItem("cart");

		actualItems = JSON.parse(actualItems);

		itemExhists = false;

		actualItems.forEach((item) => {
			if (item.product == name) {
				itemExhists = true;

				if (qty == null) {
					item.qty = item.qty + 1;
				} else {
					item.qty = qty;
				}
			}
		});

		if (!itemExhists) {
			actualItems.push(item);
		}
	} else {
		actualItems = [item];
	}

	var itemJson = JSON.stringify(actualItems);

	sessionStorage.setItem("cart", itemJson);

	itemsCount();
}

function cardLoad() {
	var cartItens = sessionStorage.getItem("cart");

	cartItens = JSON.parse(cartItens);

	var totalPrice = 0;

	if (cartItens == null || cartItens.length == 0) {
		$(".cards-wrapper").append(
			'<p class="empty-cart">Não há nenhum produto em seu carrinho</p>'
		);
	} else {
		cartItens.forEach((item) => {
			var card =
				'<div class="card"><div class="card-img"><img alt="' +
				item.product +
				'" src="assets/' +
				item.imgLocation +
				'" /></div><div class="card-content"><h3 class="product-title">' +
				item.product +
				'</h3><p class="product-price">R$ ' +
				item.price.toString().replace(".", ",") +
				'</p></div><div class="card-content"><div class="qty-remove"><input type="number" onchange="addProduct(\'' +
				item.product.trim() +
				'\', null, null, this.value);cardReload()" class="qty" min="1" max="999" value="' +
				item.qty +
				'"><button onclick="removeItem(\'' +
				item.product.trim() +
				'\')" class="remove">Remover</button></div><p class="total-price">R$ ' +
				(item.price * item.qty).toFixed(2).toString().replace(".", ",") +
				"</div></div>";

			$(".cards-wrapper").append(card);

			totalPrice = totalPrice + item.qty * parseFloat(item.price);
		});
	}

	$(".price").text("R$ " + totalPrice.toFixed(2).toString().replace(".", ","));
}

function removeItem(name) {
	if (sessionStorage.getItem("cart")) {
		actualItems = sessionStorage.getItem("cart");

		actualItems = JSON.parse(actualItems);

		var toRemove = actualItems.findIndex((item) => item.product === name);

		if (toRemove > -1) {
			actualItems.splice(toRemove, 1);

			var itemJson = JSON.stringify(actualItems);

			sessionStorage.setItem("cart", itemJson);

			cardReload();
		}
	}
}

function cardReload() {
	$(".cards-wrapper").html("");

	cardLoad();
}


function itemsCount() {
	var cartItens = sessionStorage.getItem("cart");

	cartItens = JSON.parse(cartItens);

	var totalItems = 0;

	if (cartItens.length > 0) {
		cartItens.forEach((item) => {
			totalItems = totalItems + parseInt(item.qty);
		});

		$(".number-of-items").text(totalItems);
		$(".number-of-items").addClass('nonzero');
	}
}